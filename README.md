Quarto - Game of Attributes
===========================

* Author: *Carlo E. T. Oliveira*
* Date: 2013/06/16
* Status: This is a "work in progress"
* Revision: 0.1.0
* Home: [Labase] (http://labase.selfip.org/)
* Copyright: 2013, [GPL](http://is.gd/3Udt)

Description
--------------

A development o the game of Quarto as a tutorial of techniques and tools for programming games.

The main *metaphor* is a game purchased in a newspaper stand. 
There comes a bag with a set of cardboard pieces, a sketch to assemble
and a sheet with instructions.

![Quarto Game printed Sketch](http://j.mp/quarto_sk)

Assembling
--------------
Cut carefully all parts and paint then using the pastel cakes provided with the 
*italic color indicated* .

1. Cut the four dark *peru* rectangles and paste onto the big light *navajowhite* one
2. Keep a constant padding between the rectangles
3. Paste the round *burlywood* stickers on the assigned places on the rectangles
4. Keep a constant padding between the stickers
5. Assemble the 16 pieces splitting in halves of 8 pieces
 * Eight dark *saddleblown*, Eight light *sandybrown*
 * Eight square, Eight round
 * Eight short, eight long
 * Eight hollow *maroon*, eight solid
6. Place the pieces in the outer rectangular spaces

Instructions
--------------
The game is played by two players that swap turns. The game proceed in the following steps:

1. A player select any piece in the external rectangles
2. This piece goes to the circle in the small rectangle
3. The other player place the piece in any unoccupied circle int the large square
4. If any vertical, diagonal or horizontal line contains four pieces sharing one attribute, 
this player is the winner

